#!/usr/bin/perl
use strict;
use warnings;
use open ':encoding(UTF-8)';

use constant FILE_AGE_MAX => 2; # skip files older than this number of days

my @DISTRIBUTIONS = split(" ", shift(@ARGV));
my @PARTS         = split(" ", shift(@ARGV));
my @ARCHITECTURES = split(" ", shift(@ARGV));

my $output_file;

sub save_package_info {
	my $hash = shift;

	print($output_file
		"Package: $hash->{Package}",
		"Source: $hash->{Source}",
		"Version: $hash->{Version}",
		defined $hash->{Tag} ? "Tag: $hash->{Tag}" : "",
		"Priority: $hash->{Priority}",
		"Maintainer: $hash->{Maintainer}",
		defined $hash->{Task} ? "Task: $hash->{Task}" : "",
		"Section: $hash->{Section}",
		"Description: $hash->{Description}",
		length($hash->{"Description-md5"}) == 33 ?
			"Description-md5: $hash->{\"Description-md5\"}" : "",
		"\n");
}

foreach my $distribution (@DISTRIBUTIONS) {
	foreach my $part (@PARTS) {
		my $output_filename = "Packages/Packages_${distribution}_$part";
		open($output_file, ">", $output_filename) or die "open $output_filename: $!";

		my $pkgs;

		foreach my $arch (@ARCHITECTURES) {
			my $file = "Packages/Packages_${distribution}_${part}_$arch.gz";
			if (! -f $file) { next }
			# Skip files older than FILE_AGE_MAX days
			if (-M $file > FILE_AGE_MAX) {
				print("skipped $file, too old\n");
				next;
			}
            print "### reading $file\n";

			open(my $FILE, "zcat $file |") or die "open zcat $file: $!";

			my ($lastfield, $hash);

			while (my $line = <$FILE>) {
				# "/s" lets "." match any character, even a newline
				if ($line =~ /^([\w.-]+): (.*)/s) {
					$lastfield = $1;
					$hash->{$1} = $2;
				}
				elsif ($line =~ /^ .+/) {
					$hash->{$lastfield} .= "$line";
				}
				elsif ($line =~ /^$/) {
					if (not defined $pkgs->{"$hash->{Package}-$hash->{Version}"}) {
						$pkgs->{"$hash->{Package}-$hash->{Version}"} = 1;
						if (not defined $hash->{Source}) {
							$hash->{Source} = $hash->{Package};
						}
						save_package_info($hash);
					}
					$hash = {};
					$lastfield = undef;
				}
			}

			close($FILE) or die "close $FILE: $!";
		} # arch

		close($output_file) or die "close $output_file: $!";

		system("bzip2", "-f", $output_filename) == 0
			or die "bzip2 $output_filename failed: $!";
	} # part
} # distribution
