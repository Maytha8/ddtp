#!/bin/bash -e

. "${DDTP_CONFIG_FILE}"

PROCESS_PACKAGES="./processPackages.pl"

cd "${DDTP_BASE_DIR}"

DISTRIBUTIONS="${DDTP_DISTS_SUPPORTED} ${DDTP_DISTS_TESTING} ${DDTP_DISTS_UNSTABLE}"
PARTS="main contrib"
ARCHITECTURES="amd64 armel i386 mips64el mips mipsel armhf arm64 ppc64el s390x"
MIRROR=http://ftp.de.debian.org/debian/dists

for distribution in $DISTRIBUTIONS
do
	for part in $PARTS
	do
		# Download Translation-en file if present
		file="Packages/Translation-en_${distribution}_${part}"
		echo `date`: Translation-en ${distribution}/${part}

        [ -s $file.bz2 ] && mv $file.bz2 Packages/Translation-en.bz2
		[ -s $file.xz ] && mv $file.xz Packages/Translation-en.xz
		( wget -P Packages -q -m -nd ${MIRROR}/${distribution}/${part}/i18n/Translation-en.bz2 ||
		  wget -P Packages -q -m -nd ${MIRROR}/${distribution}/${part}/i18n/Translation-en.xz ) && {
			echo `date`: Translation-en file downloaded
		} || {
            echo `date`: Failed to download Translation-en ${distribution}/${part} 1>&2
        }
        [ -s Packages/Translation-en.bz2 ] && mv Packages/Translation-en.bz2 $file.bz2
        [ -s Packages/Translation-en.xz ] && mv Packages/Translation-en.xz $file.xz

		for arch in $ARCHITECTURES
		do
			file="Packages/Packages_${distribution}_${part}_${arch}"

			echo `date`: ${distribution}/${part}/$arch
			[ -s $file.gz ] && mv $file.gz Packages/Packages.gz
			wget -P Packages -q -m -nd \
			    ${MIRROR}/${distribution}/${part}/binary-$arch/Packages.gz && {
				echo `date`: ${MIRROR}/${distribution}/${part}/binary-$arch/Packages.gz file downloaded
			} || {
				echo `date`: Failed to download ${distribution}/${part}/$arch "(${MIRROR}/${distribution}/${part}/binary-$arch/Packages.gz)" 1>&2
			}
			[ -s Packages/Packages.gz ] && mv Packages/Packages.gz $file.gz
		done
	done
done

$PROCESS_PACKAGES "$DISTRIBUTIONS" "$PARTS" "$ARCHITECTURES"
