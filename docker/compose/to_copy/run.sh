#!/bin/sh
MARKER=/srv/ddtp.debian.org/.docker_injection_is_done
if [ ! -f "$MARKER" ]; then
    cp -avr /srv/inject_on_first_run/. /srv/ddtp.debian.org/
    touch $MARKER
fi
apache2ctl -D FOREGROUND