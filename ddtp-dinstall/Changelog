2011-02-19  Nicolas François  <nicolas.francois@centraliens.net>

	* checks/ddtp_i18n_check.sh: New STABLE and TESTING names
	* tests/testsuite/good.pkgs/pristine/squeeze: Moved to 
	tests/testsuite/good.pkgs/pristine/wheezy
	* tests/testsuite/good.pkgs/package_with_multiple_versions/squeeze:
	Moved to tests/testsuite/good.pkgs/package_with_multiple_versions/wheezy
	* tests/testsuite/good.pkgs/package_with_multiple_versions-same_description/squeeze:
	Moved to tests/testsuite/good.pkgs/package_with_multiple_versions-same_description/wheezy

2009-03-31  Nicolas François  <nicolas.francois@centraliens.net>

	Fix the testsuite after the change of the TESTING name.
	* tests/testsuite/good.pkgs/pristine/lenny: Moved to
	tests/testsuite/good.pkgs/pristine/squeeze
	* tests/testsuite/good.pkgs/package_with_multiple_versions/lenny:
	Moved to tests/testsuite/good.pkgs/package_with_multiple_versions/squeeze
	* tests/testsuite/good.pkgs/package_with_multiple_versions-same_description/lenny:
	Moved to tests/testsuite/good.pkgs/package_with_multiple_versions-same_description/squeeze

2009-03-31  Nicolas François  <nicolas.francois@centraliens.net>

	* checks/ddtp_i18n_check.sh: Updated after the new Lenny release.
	* checks/ddtp_i18n_check.sh: Only generate the bz2 compressed
	files. The non compressed files are no more provided, hence the
	script should no be run twice on the same directory.

2008-08-12  Nicolas François  <nicolas.francois@centraliens.net>

	* tests/testsuite/bad/2blocks-without-md5.expected,
	tests/testsuite/bad/2blocks-without-md5,
	tests/testsuite/bad.pkgs/2blocks-without-md5: Added new test unit
	with multiple errors to tests the handling of --debug.

2008-08-12  Nicolas François  <nicolas.francois@centraliens.net>

	* tests/testsuite.sh: Send error messages to stderr.
	* tests/testsuite.sh: Check the checker's options.  Add support
	for providing checker options to run_test().  prepare_testsuite():
	Drop the lines starting with #DEBUG# in the expected logs when
	--debug is not specified.  Indicate the options used when a test
	unit fails.

2008-08-12  Nicolas François  <nicolas.francois@centraliens.net>

	* checks/ddtp_i18n_check.sh: Added support for the--dry-run option
	(do not generate the compressed files).
	* checks/ddtp_i18n_check.sh: Added option --debugto set the DEBUG
	variable.

2008-08-12  Nicolas François  <nicolas.francois@centraliens.net>

	* tests/testsuite/bad.pkgs/no-code-name,
	tests/testsuite/bad.pkgs/no-code-name1: Naming consistency:
	no-code-name renamed to no-code-name1.

2008-08-12  Nicolas François  <nicolas.francois@centraliens.net>

	* tests/testsuite/bad/no-timestamp.gpg,
	tests/testsuite/bad/no-timestamp.gpg.expected,
	tests/testsuite/bad.pkgs/no-timestamp.gpg,
	tests/testsuite/bad/timestamp.gpg-in-wrong-directory1,
	tests/testsuite/bad/timestamp.gpg-in-wrong-directory1.expected,
	tests/testsuite/bad.pkgs/timestamp.gpg-in-wrong-directory1,
	tests/testsuite/bad/no-timestamp,
	tests/testsuite/bad/no-timestamp.expected,
	tests/testsuite/bad.pkgs/no-timestamp,
	tests/testsuite/bad/timestamp-in-wrong-directory1,
	tests/testsuite/bad/timestamp-in-wrong-directory1.expected,
	tests/testsuite/bad.pkgs/timestamp-in-wrong-directory1: Added test
	units.

2008-08-12  Nicolas François  <nicolas.francois@centraliens.net>

	* tests/testsuite.sh: Keep the logs of the failing tests.

2008-08-12  Nicolas François  <nicolas.francois@centraliens.net>

	* tests/testsuite.sh: Added option --build-expected to build the
	.expected files.
	* tests/testsuite/bad/package-not-in-list,
	tests/testsuite/bad.pkgs/package-not-in-list: Added test unit.

2008-08-12  Nicolas François  <nicolas.francois@centraliens.net>

	* tests/testsuite.sh: Added function run_test.
	* tests/testsuite.sh: Use only one loop for the good and bad
	testsuites (provide the expected status to run_test ().
	* tests/testsuite.sh: Compare the log of the checker with expected
	logs (normalized to remove dates & random parts).
	* tests/testsuite/{good,bad}/*.expected: Added expected logs from
	the checker.

2008-08-12  Felipe Augusto van de Wiel  <faw@funlabs.org>

	* checks/ddtp_i18n_check.sh: Added $SPECIAL_FILES to track SHA256SUMS,
	timestamp and its GPG signature. It should ease the addition of other
	special/control files.
	* testsuite/*/*/timestamp{,.gpg}: Added in all testsuites.
	* checks/ddtp_i18n_check.sh: Remove Translation-*.{bz2,gz} when the
	script fails.
	* checks/ddtp_i18n_check.sh: Remove white space before ';;' (for
	consistency).
	* checks/ddtp_i18n_check.sh: Added diff between the list of packages
	from DAK and the list of packages in Translation files to make it
	easier to spot problems.

2008-08-11  Nicolas François  <nicolas.francois@centraliens.net>

	* tests/testsuite.sh: Fixed bug in the removal of the .svn
	directories in prepare_testsuite() when a parent directory
	contains a space.

2008-08-11  Nicolas François  <nicolas.francois@centraliens.net>

	* tests/testsuite/bad/SHA256SUMS_not_in_parent_dir/SHA256SUMS: Add
	the SHA sum of the wrong SHA256SUMS so that it fails for the good
	reason.
	* tests/testsuite/bad/directory_with_spaces/SHA256SUMS: Add a
	space to the filename in SHA256SUMS.
	* tests/testsuite/bad/file_with_spaces2/SHA256SUMS: Likewise.
	* tests/testsuite/bad/file_with_spaces1/SHA256SUMS: Likewise.
	* tests/testsuite/bad/no-code-name2/SHA256SUMS: rename the file to
	unstable (instead of sid) in the SHA256SUMS so that it fails for
	the good reason.

2008-08-11  Nicolas François  <nicolas.francois@centraliens.net>

	* tests/testsuite/bad/no-code-name3,
	tests/testsuite/bad.pkgs/no-code-name3: New test unit (testing
	instead of lenny).

2008-08-11  Nicolas François  <nicolas.francois@centraliens.net>

	* checks/ddtp_i18n_check.sh: Added missing quotes.
	* checks/ddtp_i18n_check.sh: Fixed location of SHA256SUMS.

2008-08-11  Felipe Augusto van de Wiel  <faw@funlabs.org>

	* checks/ddtp_i18n_check.sh: Added a temporary working directory, we
	now put all the generated files there and remove it with a trap.
	* checks/ddtp_i18n_check.sh: Sort SHA256SUMS before compare them.
	Using 'find' do not guarantee that we always have the same sequence
	of files.
	* checks/ddtp_i18n_check.sh: Renamed the generated lists of packages
	to follow the name of the Translation file used to generate it.

2008-08-11  Nicolas François  <nicolas.francois@centraliens.net>

	* tests/testsuite/bad/SHA256SUMS_not_in_parent_dir,
	tests/testsuite/bad/no_SHA256SUMS,
	tests/testsuite/bad.pkgs/SHA256SUMS_not_in_parent_dir,
	tests/testsuite/bad.pkgs/no_SHA256SUMS: Added new test units.

2008-08-11  Felipe Augusto van de Wiel  <faw@funlabs.org>

	* checks/ddtp_i18n_check.sh: Added an exit trap to clean *.pkgs files.
	* checks/ddtp_dinstall.sh: Clean the package list directory.

2008-08-11  Nicolas François  <nicolas.francois@centraliens.net>

	* checks/ddtp_i18n_check.sh: Added diff -au output in case of
	sha256sums mismatch.

2008-08-11  Nicolas François  <nicolas.francois@centraliens.net>

	* tests/testsuite/bad/SHA256SUMS_file_not_in_dists,
	tests/testsuite/bad/file_not_in_SHA256SUMS,
	tests/testsuite/bad/wrong_sum_in_SHA256SUMS,
	tests/testsuite/bad.pkgs/file_not_in_SHA256SUMS,
	tests/testsuite/bad.pkgs/SHA256SUMS_file_not_in_dists,
	tests/testsuite/bad.pkgs/wrong_sum_in_SHA256SUMS: Added new test
	units.

2008-08-11  Nicolas François  <nicolas.francois@centraliens.net>

	* tests/testsuite/bad/file_with_spaces1,
	tests/testsuite/bad.pkgs/file_with_spaces1,
	tests/testsuite/bad/file_with_spaces2,
	tests/testsuite/bad.pkgs/file_with_spaces2,
	tests/testsuite/bad/directory_with_spaces,
	tests/testsuite/bad.pkgs/directory_with_spaces: Added new
	testsuites.

2008-08-11  Nicolas François  <nicolas.francois@centraliens.net>

	* checks/ddtp_i18n_check.sh, tests/new_test_unit.sh,
	tests/testsuite.sh: Added Id keyword expansion.

2008-08-11  Felipe Augusto van de Wiel  <faw@funlabs.org>

	* checks/ddtp_dinstall.sh: Added script to atomically rotate
	directories to DAK.

2008-08-11  Nicolas François  <nicolas.francois@centraliens.net>

	* checks/ddtp_i18n_check.sh, tests/new_test_unit.sh,
	tests/testsuite.sh: Fixed quotes.
	* tests/testsuite/good.pkgs/parent directory with spaces,
	testsuite/good/parent directory with spaces: Added new test unit.

2008-08-10  Felipe Augusto van de Wiel  <faw@funlabs.org>

	* checks/ddtp_i18n_check.sh: Commented out $STABLE and removed its
	usage from the affected code sections.
	* checks/ddtp_i18n_check.sh: Added quotes to some variables.
	* checks/ddtp_i18n_check.sh: Added comments to clarify some actions.
	* checks/ddtp_i18n_check.sh: Added test to validate the existent
	SHA256SUMS and the new generated one.
	* tests/testsuite/bad/no-code-name2,
	tests/testsuite/bad.pkgs/no-code-name2: Added new test.
	* tests/testsuite/good.pkgs/package_with_multiple_versions-same_description/sid:
	renamed etch to sid.
	* tests/testsuite/good.pkgs/package_with_multiple_versions/sid:
	renamed	etch to sid.
	* tests/testsuite/bad/*/SHA256SUMS: Added missing SHA256SUMS.
	* tests/testsuite/good/*/SHA256SUMS: Added missing SHA256SUMS.

2008-08-10  Felipe Augusto van de Wiel  <faw@funlabs.org>

	* tests/testsuite.sh: Do not remove SHA256SUMS.
	* tests/testsuite/good/pristine/SHA256SUMS: Added.
	* tests/testsuite/good.pkgs/pristine/etch: Renamed to sid.
	* tests/testsuite/*/*/dists/etch/: Renamed to sid.

2008-08-10  Felipe Augusto van de Wiel  <faw@funlabs.org>

	* checks/ddtp_i18n_check.sh: Commented out stable from the list of
	valid filenames.
	* checks/ddtp_i18n_check.sh: Not removing SHA256SUMS, since we are
	now checking if files are valid.

2008-08-10  Nicolas François  <nicolas.francois@centraliens.net>

	* tests/testsuite.sh: Updated testsuite script for the new
	repository layout.

2008-08-09  Nicolas François  <nicolas.francois@centraliens.net>

	* checks: ddtp_i18n_check.sh moved to the checks/ directory
	* tests: testsuite, testsuite.sh, and new_test_unit.sh moved to
	tests.

2008-08-09  Nicolas François  <nicolas.francois@centraliens.net>

	* testsuite.sh: Run the testsuite with -eu
	* testsuite.sh: Run with a temporary directory created with
	mktemp.

2008-08-09  Nicolas François  <nicolas.francois@centraliens.net>

	* testsuite/: Added testsuites: bad/wrong-language-name,
	bad/translation-file-in-wrong-directory1,
	bad/translation-file-in-wrong-directory2,
	bad/translation-file-in-wrong-directory3, bad/no-code-name,
	good/package_with_multiple_versions,
	good/package_with_multiple_versions-same_description,
	bad/bad-directory, bad/wrong-encoding-one-block

2008-08-09  Nicolas François  <nicolas.francois@centraliens.net>

	* testsuite.sh: Added Copyright and license header.

2008-08-09  Nicolas François  <nicolas.francois@centraliens.net>

	* new_test_unit.sh: Added script to create a new tests unit based
	on pristine.

2008-08-09  Nicolas François  <nicolas.francois@centraliens.net>

	* testsuite.sh: Make the testsuite generate some $test.log and
	output a simple list of tests which succeeded and failed.
	* testsuite.sh: Remove the .log files at the beginning.
	* testsuite.sh: Merge some common stuff from the processing of
	"good" and "bad" to the prepare_testsuite() and clean_testsuite()
	functions.

2008-08-09  Nicolas François  <nicolas.francois@centraliens.net>

	* ddtp_i18n_check.sh: run with set -u.
	* ddtp_i18n_check.sh: Add "|| false" at the end of the "while
	read". The while will just fail if an internal check "exit 1", but
	the script is not exited. "|| false" makes the script fail (and
	exit) in that case.

2008-08-08  Nicolas François  <nicolas.francois@centraliens.net>

	Thanks for the patience of Felipe Augusto van de Wiel and Michael
	Bramer.
	* ddtp_i18n_check.sh: Add indication and date in case of success
	* ddtp_i18n_check.sh: Move the diff | grep inside the if (failures
	caused the script to exit).
	* ddtp_i18n_check.sh: Add slashes to match suite name more
	strictly.
	* ddtp_i18n_check.sh: Run the script in C locale.
	* ddtp_i18n_check.sh: Enhance the error messages from the
	has_valid_fields() function.

2008-08-08  Nicolas François  <nicolas.francois@centraliens.net>

	Initial release.
