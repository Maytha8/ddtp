use lib '/srv/ddtp.debian.org/ddts/bin/';
use ddts_config;
use ddts_common;

use DBI;
use Digest::MD5 qw(md5_hex);
use Encode;

use MIME::Parser;
use MIME::Entity;
use Text::Iconv;

my @DSN = ("DBI:Pg:service=ddtp", "", "");

my $dbh = DBI->connect(@DSN,
    { PrintError => 0,
      RaiseError => 1,
      AutoCommit => 0,
    });

die $DBI::errstr unless $dbh;

# regex

my $mMD5  = qr/([a-zA-Z0-9]+)/;
my $mLANG = qr/\b([a-z][a-z](?(?=_)_[A-Z][A-Z]))\b/;
my $mCHAR = qr/(?:\.([a-zA-Z0-9-]+))?/;
my $mPKG  = qr/([\w+.-]+)/;
my $mNB   = qr/(\d+)/;
my $mADDR = qr/([\w+.@-]+)/;
my $mADDRwithUserName = qr/([ "\<\>\(\)\w+\.\@-]+)/;

# Number of days before we ignore the owner when sending descriptions to translate
# Must be greater than zero, otherwise we'll keep fetching the same description over and over
my $OWNER_EXPIRE = 15;

# This is _copied_ from /srv/ddtp.debian.org/ddts.cgi
sub get_versions ($) {
	my $desc_id = shift;

	my @versions=();

	my $sth = $dbh->prepare("SELECT package,version FROM package_version_tb WHERE description_id=?");
	$sth->execute($desc_id);

	my $package;
	my $version;
	while(($package,$version) = $sth->fetchrow_array) {
		push @versions,"$package ($version)";
	}
	return (@versions);
}

# This is _copied_ from /srv/ddtp.debian.org/ddts/bin/mailparser.pl
sub desc_to_parts ($) {
    my $desc = shift;

    my @parts;
    my $part;
    my @lines=split(/\n/, $desc);

    foreach (@lines) {
        if (not @parts) {
            push @parts,$_;
            $part="";
            next;
        }
        if ($_ ne " .") {
            $part.=$_;
            $part.="\n";
        } else {
            push @parts,$part if ($part ne "");
            $part="";
        }
    }
    push @parts,$part if ($part ne "");

    return @parts;
}

# This is _copied_ from /srv/ddtp.debian.org/ddts/bin/mailparser.pl
sub own_a_description ($$$) {
	my $desc_id = shift;
	my $lang = shift;
	my $owner = shift;

	if ($owner eq "") {
		return 0;
	}

	eval {
		$dbh->do("DELETE FROM owner_tb WHERE description_id = ? AND language = ? AND lastsend < (CURRENT_DATE - ?::int4)", undef,
					$desc_id, $lang, $OWNER_EXPIRE );
		$dbh->do("INSERT INTO owner_tb (description_id,language,owner,lastsend) VALUES (?,?,?,CURRENT_DATE);", undef, $desc_id, $lang, $owner);
		$dbh->commit;   # commit the changes if we get this far
	};
	if ($@) {
		$dbh->rollback; # undo the incomplete changes
		return 1;	# Failed, translation just given to someone else
	}
	return 0;
}

# This is _copied_ from /srv/ddtp.debian.org/ddts/bin/mailparser.pl
sub add_description_to_db {
	my $from_address= shift(@_);
	my $description= shift(@_);
	my $description_trans= shift(@_);
	my $lang_postfix= shift(@_);
	
	my $md5sum_description;
	my $md5sum_description_trans;
	my $package="";
	my $first_package_char="";
	my $db_item_dir;
	my $db_item_name;
	my $db_item_from;
	my $db_item_new_name;
	my $db_item_new_from;

	my $return_db;
	my $sec;
	my $old_from;
	my @packagelist;

	my $description_id;

	my $not_add_to_db=0;

	debug "",
	      "from_address=$from_address",
	      "description=$description",
	      "description_trans=$description_trans",
	      "lang_postfix=$lang_postfix";

	$md5sum_description=get_md5_hex($description);
	$md5sum_description_trans=get_md5_hex($description_trans);

	debug "md5sum_description=$md5sum_description";

	sub get_description_id {
		my $md5sum= shift(@_);

		my $description_id;

		my $sth = $dbh->prepare("SELECT description_id FROM description_tb WHERE description_md5=?");
		$sth->execute($md5sum);
		($description_id) = $sth->fetchrow_array;
		return $description_id;
	}

	sub get_translation_id {
		my $description_id= shift(@_);
		my $lang= shift(@_);

		my $translation_id;

		my $sth = $dbh->prepare("SELECT translation_id FROM translation_tb WHERE description_id=? and language=?");
		$sth->execute($description_id,$lang);
		($translation_id) = $sth->fetchrow_array;
		return $translation_id;
	}

	sub get_description {
		my $description_id= shift(@_);

		my $description;

		my $sth = $dbh->prepare("SELECT description FROM description_tb WHERE description_id=?");
		$sth->execute($description_id);
		($description) = $sth->fetchrow_array;
		return $description;
	}

	sub get_part_id {
		my $part_md5= shift(@_);
		my $lang= shift(@_);

		my $part_id;

		my $sth = $dbh->prepare("SELECT part_id FROM part_tb WHERE part_md5=? and language=?");
		$sth->execute($part_md5,$lang);
		($part_id) = $sth->fetchrow_array;
		return $part_id;
	}

	sub save_parts_to_db {
		my $md5sum= shift(@_);
		my $part  = shift(@_);
		my $lang  = shift(@_);

		if ($part and $md5sum) {
			eval {
				my $part_id=get_part_id($md5sum,$lang);
				if ($part_id) {
					$dbh->do("UPDATE part_tb SET part = ? WHERE part_id = ?;", undef, $part, $part_id);
				} else {
					$dbh->do("INSERT INTO part_tb (part_md5, part, language) VALUES (?,?,?);", undef, 
						$md5sum,$part,$lang);
				}
				$dbh->commit;   # commit the changes if we get this far
			};
			if ($@) {
				warn "Transaction aborted because $@";
				$dbh->rollback; # undo the incomplete changes
				print $part . "\n" ;
			}
		}
	}

	my $translation_orig=$description_trans;
	$description_id=get_description_id($md5sum_description);
	if ($description_id) {
		eval {
			my $translation_id=get_translation_id($description_id,$lang_postfix);
			# FIXME:  add update a translation
			#         update only if sender is owner
			#         etc.
			if (not $translation_id) {
				$dbh->do("INSERT INTO translation_tb (description_id, translation, language) VALUES (?,?,?);", undef, $description_id,$description_trans,$lang_postfix);
			} else {
				debug "UPDATE translation_tb SET translation='$description_trans' WHERE description_id='$description_id' and language='$lang_postfix';";
				$dbh->do("UPDATE translation_tb SET translation=? WHERE description_id=? and language=?;", undef,
					$description_trans, $description_id, $lang_postfix);
			}
			$dbh->commit;   # commit the changes if we get this far
		};
		if ($@) {
			warn "Transaction aborted because $@";
			$dbh->rollback; # undo the incomplete changes
		} else {
			status	"add the translation in the db";
			status	"see https://ddtp.debian.org/ddt.cgi?desc_id=$description_id&language=$lang_postfix";
			status	"";
			own_a_description($description_id,$lang_postfix,$from_address);
		}
	} else {
		status	"the package description was not found in the db";
		status	"";
	}

	if ($description_id) {
		my @t_parts = desc_to_parts($translation_orig);
		my @e_parts = desc_to_parts($description);

		my @e_parts_md5;
		foreach (@e_parts) {
			push @e_parts_md5,get_md5_hex($_);
		}

		if ($#e_parts_md5 = $#t_parts) {
			my $a=0;
			while ($a <= $#e_parts_md5 ) {
				&save_parts_to_db($e_parts_md5[$a],$t_parts[$a],$lang_postfix);
				$a++
			}
		}
	}

	if (not $description_id) {
		return -1;
	} else {
		return 0;
	}
}

# Modified version from DDTS. Takes an optional package name.
sub get_untrans {
	my $lang_postfix= shift(@_);
	my $from= shift(@_);
	my $package= shift(@_) || undef;
	my $force= shift(@_) || 0;     # If force=0 we'll skip already translated things (not applied to package=undef)
	
	my @files;

	debug "",
	      "from=$from",
	      "package=$package",
	      "lang_postfix=$lang_postfix";

	my $sth;
	# If the package is a number, we assume it's a description ID. If
	# it's a defined string we read it as a package name. If it's
	# undefined we pick a random package
	if( defined $package )
	{
	  if( $package =~ /^\d+$/ )
	  {
	    $sth = $dbh->prepare("SELECT description_id FROM description_tb d ".
	                         "WHERE description_id = ?");
	    $sth->execute($package);
	  }
	  else
	  {
	    $sth = $dbh->prepare("SELECT DISTINCT description_id FROM package_version_tb ".
	                         "WHERE description_id in (SELECT description_id FROM active_tb) and package=?");
	    $sth->execute($package);
	  }
	}
	else
	{
	  $sth = $dbh->prepare("SELECT description_id FROM description_tb ".
	                       "WHERE description_id in (SELECT description_id FROM active_tb) ".
	                       "and description_id not in (SELECT description_id FROM translation_tb WHERE language=?) ".
	                       "and description_id not in (SELECT description_id FROM owner_tb ".
	                                                  "WHERE language=? AND lastsend >= (CURRENT_DATE - ?::int4)) ".
	                                                  "ORDER BY prioritize DESC LIMIT 100");
          $sth->execute($lang_postfix, $lang_postfix, $OWNER_EXPIRE);
        }

	my $description_id;
	my $found = 0;
	my $locked = 0;
	my @all_descr;

	while(($description_id) = $sth->fetchrow_array) {
	  # If not in force mode we need to check that this isn't translated
	  print STDERR "Found description ID $description_id\n";
	  push @all_descr, $description_id;
	  $found++;
	  if( not $force )
	  {
	    my ($done) = $dbh->selectrow_array( "SELECT EXISTS (SELECT 1 from translation_tb WHERE description_id = ? AND language=?)", undef,
					      $description_id, $lang_postfix);
	    next if $done; 
	  }
	  # We check the return value because a parallel email might have just given the description to someone else
	  if( own_a_description($description_id,$lang_postfix,$from) == 0 )
	  {
	    push @files, $description_id;
	    last;
	  }
	  else 
	  { 
	    $locked = 1;
	    if( $force )
	    {
  	      push @files, $description_id;
  	      last;
	    }
	  }
	}
	my $body;
	if ($#files >= 0) {
		$body = get_untrans_file_from_db($lang_postfix,$from,@files);
	} elsif( not $found ) {
		status "ERROR: The server doesn't find a package with this name ($package)!";
		die "Package $package not found\n";
	} elsif( $locked ) {
		status "WARNING: All found descriptions locked (force=$force)";
		die "All descriptions for package $package already fetched by others\n";
	} else {
		status "WARNING: All found descriptions skipped (force=$force)";
		foreach my $id (@all_descr) {
			$id = qq(<a href="https://ddtp.debian.org/ddt.cgi?desc_id=$id&language=$lang_postfix">$id</a>);
		}
		die "All descriptions for package $package already translated (checked ".join(",",@all_descr).")\n";
	}
	return ($body);
}

sub get_untrans_file_from_db {
	my $lang_postfix= shift(@_);
	my $from= shift(@_);
	my $description_id= shift(@_); # was my $file= shift(@_);

	debug "",
	      "lang_postfix=$lang_postfix",
	      "$description_id";

	my $body = "";

	my $translation;

	my $sth = $dbh->prepare("SELECT A.description,A.prioritize,A.package,A.source,B.description_id,O.owner FROM (description_tb AS A LEFT JOIN active_tb AS B ON A.description_id=B.description_id) LEFT JOIN (SELECT owner,description_id FROM owner_tb WHERE language=?) AS O ON A.description_id=O.description_id WHERE A.description_id=?");
	$sth->execute($lang_postfix,$description_id);

	my ($description,$prioritize,$package,$source,$active,$owner) = $sth->fetchrow_array ;

	my @versions=get_versions($description_id);

	$body .="\n"
		."# Source: $source\n"	
		."# Package(s): $package\n"	
		."# Prioritize: $prioritize\n"	
		."# Versions: " . join(", ",@versions) . "\n";
	if ($active) {
		$body .= "# This Description is active\n";
	}
	if ($owner) {
		$body .= "# This Description is owned\n";
	}
	$body .= "Description: $description";	

	my @parts=desc_to_parts($description);

	my $num=0;
	foreach (@parts) {
		my $part_md5=get_md5_hex($_);
		$sth = $dbh->prepare("SELECT part FROM part_tb WHERE part_md5=? and language=?");
		$sth->execute($part_md5,$lang_postfix);

		my ($part) = $sth->fetchrow_array ;
		if ($num == 0) {
			$body .= "Description-$lang_postfix: ";
		}
		if ($part) {
			$body .= "$part";
			if ($num == 0) {
				$body .= "\n";
			}
		} else {
			if ($num == 0) {
				$body .= "<trans>\n";
			} else {
				$body .= " <trans>\n";
			}
		}
		$num+=1;
		if ( ($num>1) and ($num<=$#parts) ) {
			$body .= " .\n";
		}
	}

	$body .= "#\n";	
	$body .= "# other Descriptions of the $package package with a translation in $lang_postfix:\n";	
	$body .= "# \n";	

	my $description_id2;

	$sth = $dbh->prepare("SELECT A.description_id,B.description_id FROM description_tb AS A LEFT JOIN active_tb AS B ON A.description_id=B.description_id WHERE A.package=? and A.description_id in (SELECT description_id FROM translation_tb WHERE language=?) ORDER BY A.description_id DESC");
	$sth->execute($package,$lang_postfix);

	while(($description_id2,$active) = $sth->fetchrow_array) {
		if ($description_id2 ne $description_id) {
			$body .= "# Description-id: $description_id2 https://ddtp.debian.org/ddt.cgi?desc_id=$description_id2\n";	
			$body .= "# patch https://ddtp.debian.org/ddt.cgi?diff1=$description_id2&diff2=$description_id&language=$lang_postfix\n";	

			if ($active) {
				$body .= "# This Description is active\n";
			}

			my $sth2 = $dbh->prepare("SELECT tag,date_begin,date_end FROM description_tag_tb WHERE description_id=?");
			$sth2->execute($description_id2);

			while(my($tag,$date_begin,$date_end) = $sth2->fetchrow_array) {
				$body .= "# This Description was in $tag from $date_begin to $date_end;\n";
			}
	        $body .= "# \n";
		}
	}
	return $body;
}

sub add_suggestion_to_db {
	my $package= shift(@_);
	my $version= shift(@_);
	my $description_md5= shift(@_);
	my $translation= shift(@_);
	my $language= shift(@_);
	my $importer= shift(@_);
	
	my $oldtranslation;

	# get the description_id
	my $d_id;
	my $sth = $dbh->prepare("SELECT description_id FROM description_tb WHERE description_md5=?");
	$sth->execute($description_md5);
	($d_id) = $sth->fetchrow_array;

	# get the translation
	$sth = $dbh->prepare("SELECT translation FROM translation_tb WHERE description_id=? and language=?");
	$sth->execute($d_id,$language);
	($oldtranslation) = $sth->fetchrow_array;

	if ((defined($oldtranslation) and ($importer eq "rosetta"))) {
		my $fh;
        open ($fh, ">>/tmp/ddts.log") or return undef;
		print $fh "           md5: $description_md5\n";
		print $fh "       package: $package\n";
		print $fh "   translation: $translation\n";
		print $fh "oldtranslation: $oldtranslation\n";
		print $fh "Translation is already translated\n\n";
		print $fh "\n\n";
		close $fh;
		return "Translation is translated";
	}

	my $body = get_untrans_file_from_db($language,"",$d_id);
	if (not (defined($oldtranslation))) {
                ($oldtranslation) = ($body =~ /^Description-[^:]+: (.*\n(?: .*\n)+)/m);
	}

	if ( $oldtranslation !~ /^Description/ ) {
		$oldtranslation="Description-$language: ".$oldtranslation;	
	}
	if ( $translation !~ /^Description/ ) {
		$translation="Description-$language: ".$translation;	
	}

	my $fh;
    open ($fh, ">>/tmp/ddts.log") or return undef;
	print $fh "           md5: $description_md5\n";
	print $fh "       package: $package\n";
	print $fh "   translation: $translation\n";
	print $fh "oldtranslation: $oldtranslation\n";
	print $fh "\n\n";
	close $fh;

	if ($oldtranslation eq $translation) {
		return "Translation don't have any changes";
	} else {
		eval {
			$dbh->do("INSERT INTO suggestion_tb (package, version, description_md5, translation, language, importer, importtime) VALUES (?,?,?,?,?,?,CURRENT_DATE);", undef, $package,$version,$description_md5,$translation,$language,$importer);
			$dbh->commit;   # commit the changes if we get this far
		};
	}

	return undef;
}

sub get_suggestions {
	my $language= shift(@_);

    my @suggestions=();

    my $sth = $dbh->prepare("SELECT suggestion_id,package,description_md5,importer,importtime FROM suggestion_tb WHERE language=?");
    $sth->execute($language);

    my ($id,$package,$md5,$importer,$importtime);
    while(($id,$package,$md5,$importer,$importtime) = $sth->fetchrow_array) {
        push @suggestions, [$id, $package, $md5, $importer, $importtime];
    }
    return (@suggestions);
}

sub get_suggestion_infos {
	my $suggestion_id= shift(@_);

	my %suggestion;

    my $sth = $dbh->prepare("SELECT package,version,description_md5,translation,language,importer,importtime FROM suggestion_tb WHERE suggestion_id=?");
    $sth->execute($suggestion_id);

	my ($package,$version,$description_md5,$translation,$language,$importer,$importtime);
    while(($package,$version,$description_md5,$translation,$language,$importer,$importtime) = $sth->fetchrow_array) {
		$suggestion{'package'}=$package;
		$suggestion{'version'}=$version;
		$suggestion{'description_md5'}=$description_md5;
		$suggestion{'suggestion'}=$translation."\n";
		$suggestion{'language'}=$language;
		$suggestion{'importer'}=$importer;
		$suggestion{'importtime'}=$importtime;
    }

	# get the description
	$sth = $dbh->prepare("SELECT description_id,description,prioritize,package,source FROM description_tb WHERE description_md5=?");
	$sth->execute($suggestion{'description_md5'});
	($suggestion{'description_id'},$suggestion{'description'},$suggestion{'prioritize'},$suggestion{'packages'},$suggestion{'source'}) = $sth->fetchrow_array;

	my @versions=get_versions($suggestion{'description_id'});
	$suggestion{'versions'}= join(", ",@versions);

	# get the translation
	$sth = $dbh->prepare("SELECT translation FROM translation_tb WHERE description_id=? and language=?");
	$sth->execute($suggestion{'description_id'},$suggestion{'language'});
	($suggestion{'translation'}) = $sth->fetchrow_array;

	my $body = get_untrans_file_from_db($suggestion{'language'},"",$suggestion{'description_id'});
	if (not (defined($suggestion{'translation'}))) {
        ($suggestion{'translation'}) = ($body =~ /^Description-[^:]+: (.*\n(?: .*\n)+)/m);
	}
	if ( $suggestion{'description'} !~ /^Description: / ) {
		$suggestion{'description'}='Description: '.$suggestion{'description'};	
	}
	if ( $suggestion{'translation'} !~ /^Description/ ) {
		$suggestion{'translation'}="Description-$suggestion{'language'}: ".$suggestion{'translation'};	
	}
	if ( $suggestion{'suggestion'} !~ /^Description/ ) {
		$suggestion{'suggestion'}="Description-$suggestion{'language'}: ".$suggestion{'suggestion'};	
	}
    return (%suggestion);
}

sub get_translation_by_md5 {
	my $description_md5= shift(@_);
	my $language= shift(@_);

	my $translation;

	# get the description_id
	my $d_id;
	my $sth = $dbh->prepare("SELECT description_id FROM description_tb WHERE description_md5=?");
	$sth->execute($description_md5);
	($d_id) = $sth->fetchrow_array;

	# get the translation
	$sth = $dbh->prepare("SELECT translation FROM translation_tb WHERE description_id=? and language=?");
	$sth->execute($d_id,$language);
	($translation) = $sth->fetchrow_array;

	return $translation;
}

sub get_commonpackages_by_text {
	my @commonpackages;

	my $description=shift;

	sub get_description_id {
		my $md5sum= shift(@_);

		my $description_id;

		my $sth = $dbh->prepare("SELECT description_id FROM description_tb WHERE description_md5=?");
		$sth->execute($md5sum);
		($description_id) = $sth->fetchrow_array;
		return $description_id;
	}

	my $md5sum_description=get_md5_hex($description);
	my $description_id=get_description_id($md5sum_description);

	my @parts = desc_to_parts($description);
	my $count=0;
	foreach (@parts) {
		my $sth = $dbh->prepare("SELECT description_id FROM part_description_tb WHERE part_md5=?");
		$sth->execute(get_md5_hex($_));

		my $desc_id;
		my $count_common=0;
		my $commonpackage=" $count : ";
		while(($desc_id) = $sth->fetchrow_array) {
			next if ($desc_id eq $description_id) ;
			my @versions=get_versions($desc_id);
			$count_common++;
			$commonpackage.=join(", ",@versions) . "; ";
		}
        push @suggestions, [$id, $package, $md5, $importer, $importtime];
		if ($commonpackage eq " $count : ") {
			$commonpackage.=" no other package description; ";
		}
        push @commonpackages, [$count_common, $commonpackage];
		$count++;
	}

	return (@commonpackages);
}

1;
